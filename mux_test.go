package mux

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
)

var server *http.Server

func ServeUp() {
	if server == nil {
		server = new(http.Server)
		server.Addr = "localhost:8000"
		go server.ListenAndServe()
	}
}

type TestPathModule struct{}

func (m *TestPathModule) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	context := GetContext(req)
	fmt.Fprintf(res, "%q", context.Path)
}

func TestPathRoute(t *testing.T) {
	ServeUp()

	m := new(TestPathModule)

	r := Path("/what/is/this")
	r.Handler = m

	router := NewRouter(r)
	server.Handler = router

	var tests = []httpTest{
		{"http://localhost:8000/what/is/this", `["what" "is" "this"]`, 200},
		{"http://localhost:8000/what/is/this/", `["what" "is" "this"]`, 200},
		{"http://localhost:8000/what/is/this?madness=madness", `["what" "is" "this"]`, 200},
		{"http://localhost:8000/what/is/this/i/dont/even", `["what" "is" "this" "i" "dont" "even"]`, 200},
		{"http://localhost:8000/plerg/", ``, 404},
		{"http://localhost:8000/plerg/what/is/this1", ``, 404},
	}

	doTestRequests(t, tests)

}

type TestMatchModule struct{}

func (m *TestMatchModule) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	context := GetContext(req)
	fmt.Fprintf(res, context.Matches["page"])
}

type httpTest struct {
	url        string
	want       string
	statusCode int
}

func TestMatchRoute(t *testing.T) {
	ServeUp()

	m := new(TestMatchModule)

	r := Pass().Add(Match(`^/(?P<page>[0-9a-z\-]+)/?$`))
	r.Handler = m

	router := NewRouter(r)
	server.Handler = router

	var tests = []httpTest{
		{"http://localhost:8000/route/", "route", 200},
		{"http://localhost:8000/route", "route", 200},
		{"http://localhost:8000/this-is-a-toast", "this-is-a-toast", 200},
		{"http://localhost:8000/", "", 404},
	}

	doTestRequests(t, tests)

}

func doTestRequests(t *testing.T, tests []httpTest) {
	for _, test := range tests {
		res, err := http.Get(test.url)
		if err != nil {
			t.Log(err)
			t.FailNow()
		}

		if res.StatusCode != test.statusCode {
			t.Errorf("bad status code: got %d want %d", res.StatusCode, test.statusCode)
		}

		actual, err := ioutil.ReadAll(res.Body)
		if err != nil {
			t.Log(err)
			t.FailNow()
		}
		res.Body.Close()

		if string(actual) != test.want && test.statusCode == 200 {
			t.Errorf("bad response got:%q want:%q", actual, test.want)
			t.Fail()
		}
	}
}
