package mux

import (
	"log"
	"errors"
	"net/http"
)

type Predicate func(*http.Request) bool
type Operator func(*http.Request, *Route)
type HandleFunc func(http.ResponseWriter, *http.Request)

type Route struct {
	Name      string
	Operator  Operator
	Predicate Predicate
	Handler   http.Handler

	children []*Route
}

type Context struct {
	Matches map[string]string
	RedirectURL string
	Path    []string
}

var contexts map[*http.Request]*Context

var (
	ErrNoContext = errors.New("no http request context")
)

func newContext() (c *Context) {
	c = new(Context)
	c.Matches = make(map[string]string)
	return
}

type Router struct {
	root *Route
}

func NewRouter(root *Route) (r *Router) {
	r = new(Router)
	if contexts == nil {
		contexts = make(map[*http.Request]*Context)
	}
	r.root = root
	return
}

func (r *Router) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	contexts[req] = newContext()
	handler := r.route(r.root, res, req)
	if handler != nil {
		handler.ServeHTTP(res, req)
	}else{
		http.NotFound(res, req)
	}
	delete(contexts, req)
}

func GetContext(req *http.Request) *Context {
	c, e := contexts[req]
	if !e {
		panic(ErrNoContext)
	}
	return c
}

func (r *Router) Visualise() {
	r.root.Visualise(0);
}

func (r *Router) route(route *Route, res http.ResponseWriter, req *http.Request) http.Handler {

	if route.Predicate != nil {
		if !route.Predicate(req) {
			return nil
		}
	}

	if route.Operator != nil {
		route.Operator(req, route)
	}

	if route.Handler != nil {
		return route.Handler
	} else if len(route.children) > 0 {
		for _, child := range route.children {
			if handler := r.route(child, res, req); handler != nil {
				return handler
			}
		}
	}

	return nil
}

func (r *Route) Add(c *Route) *Route {
	r.children = append(r.children, c)
	return c
}

func (r *Route) AddChild(c *Route) *Route {
	r.children = append(r.children, c)
	return r
}

func NewRoute() (r *Route) {
	r = new(Route)
	return
}

func (r *Route) String() string {
	return r.Name
}

type ErrorHandler struct {
	error string
	code  int
}

func NewErrorHandler(error string, code int) (e *ErrorHandler) {
	e = new(ErrorHandler)
	e.error = error
	e.code = code
	return
}

func (e *ErrorHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	log.Printf("%d %s %s", e.code, e.error, req.URL.Path)
	http.Error(res, e.error, e.code)
}

type FuncHandler struct {
	f HandleFunc
}

func NewFuncHandler(f HandleFunc) (h *FuncHandler) {
	h = new(FuncHandler)
	h.f = f
	return
}

func (h *FuncHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	h.f(res, req)
}

type RedirectHandler struct {
	url string
	code int
}

func NewRedirectHandler(url string, code int) (h *RedirectHandler) {
	h = new(RedirectHandler)
	h.url = url
	h.code = code
	return
}

func (h *RedirectHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	log.Printf("%d %s %s", h.code, req.URL.Path, h.url)
	http.Redirect(res, req, h.url, h.code)
}

