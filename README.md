## Synopsis

Small tree structured muxer loosely based around the
[gorilla muxer](http://www.gorillatoolkit.org/pkg/mux).

Each route has a predicate and operator function. The predicate
function determines if the flow will continue. The operator function
modifies the internal state of the router.

## Code Example

    package main
    
    import (
    	"darfk/mux"
    	"darfk/page"
    	"net/http"
    )
    
    func main() {

	    trailingSlashRoute := mux.NewRoute().RemoveTrailingSlashRedirect()

    	pageRoute := mux.NewRoute().Get().Match(`^/(?P<page>[0-9a-z\-]*?)?/?$`)
    	pageRoute.Handler = mux.NewFuncHandler(func(res http.ResponseWriter, req *http.Request) {
    		context := mux.GetContext(req)
    		slug := context.Matches["page"]
    		if slug == "" {
    			slug = "index"
    		}
    
    		if !page.Exists(slug) {
    			http.NotFound(res, req)
    			return
    		}
    
    		page := page.NewPage("main").FromFile(slug)
    		page.Execute(res)
    	})
    
    	staticRoute := mux.NewRoute()
    	staticRoute.Name = "static route"
    	staticRoute.Handler = http.FileServer(http.Dir("static"))
    
    	router := mux.NewRouter(mux.Root(trailingSlashRoute, pageRoute, staticRoute))
    
    	router.Visualise()
    
    	server := new(http.Server)
    	server.Addr = "localhost:8000"
    	server.Handler = router
    	server.ListenAndServe()
    }

## Motivation

I wanted to have a go at writing my own muxer.

## Installation

`go get github.com/darfk/mux`

## Tests

`go test github.com/darfk/mux`

## License

MIT