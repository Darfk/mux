package mux

import (
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strings"
)

func (in *Route) Match(reString string) (r *Route) {
	re := regexp.MustCompile(reString)

	r = NewRoute()

	r.Predicate = func(req *http.Request) bool {
		return re.MatchString(req.URL.Path)
	}

	r.Operator = func(req *http.Request, route *Route) {

		context := GetContext(req)

		keys := re.SubexpNames()
		if len(keys) > 1 {
			keys = keys[1:]
		}

		values := re.FindStringSubmatch(req.URL.Path)
		if len(values) > 1 {
			values = values[1:]
		}

		if len(keys) > 0 && len(keys) == len(values) {
			for i := range keys {
				context.Matches[keys[i]] = values[i]
			}
		}

	}

	r.Name = `match ` + reString

	in.Add(r)

	return
}

func (in *Route) Path(path string) (r *Route) {

	r = NewRoute()

	r.Predicate = func(req *http.Request) bool {
		return strings.Index(req.URL.Path, path) == 0
	}

	r.Operator = func(req *http.Request, route *Route) {
		GetContext(req).Path = strings.Split(strings.Trim(req.URL.Path, "/"), "/")
	}

	r.Name = `match path ` + path
	
	in.Add(r)

	return 
}

func Root(c ...*Route) (r *Route) {
	r = new(Route)
	for i := range c {
		r.Add(c[i])
	}

	r.Name = `root`
	return
}

func (in *Route) Method(method string) (r *Route) {
	r = new(Route)
	r.Predicate = func(req *http.Request) bool {
		return req.Method == method
	}
	in.Add(r)
	return
}

func (in *Route) Get() (r *Route) {
	return in.Method("GET")
}

func (in *Route) Post() (r *Route) {
	return in.Method("POST")
}

func (in *Route) RemoveTrailingSlashRedirect() (r *Route) {
	r = new(Route)
	r.Name = "trailing slash remover"
	r.Predicate = func(req *http.Request) bool {
		return len(req.URL.Path) > 1 && strings.HasSuffix(req.URL.Path, "/")
	}
	r.Operator = func(req *http.Request, route *Route) {
		route.Handler = NewRedirectHandler("/" + strings.Trim(req.URL.Path, "/"), 301)
	}
	in.Add(r)
	return
}

func (in *Route) Log() (r *Route) {
	r = NewRoute()

	r.Operator = func(req *http.Request, route *Route) {
		log.Println(req)
	}
	
	r.Name = `log`
	in.Add(r)
	return
}

func (r *Route) Visualise(depth int) {
	fmt.Printf("%s- %s\n", strings.Repeat(" ", depth), r)
	for _, child := range r.children {
		child.Visualise(depth + 1)
	}
}
